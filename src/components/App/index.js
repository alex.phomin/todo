import React from "react";
import SearchPanel from "../SearchPanel";
import TodoList from "../TodoList";
import './index.css';
import AppHeader from "../AppHeader";
import StatusFilter from "../StatusFilter";
import AddItem from "../AddItem";
import {Col, Row} from "antd";
import { Input } from 'antd';

class App extends React.Component {

    maxId = 200;

    state =  {
        todoData : [
            {name: 'Drink Coffee', isImportant: false, done: false, id: 1},
            {name: 'Learn React', isImportant: true, done: false, id: 2},
            {name: 'Build React App', isImportant: true, done: false, id: 3}
        ],
        search: '',
        filter: 'all'
    };

    deleteItem = (id) => {
        this.setState(({ todoData }) => {
            const idx = todoData.findIndex((el) => el.id === id);
            const beforeArray = todoData.slice(0, idx);
            const afterArray = todoData.slice(idx+1);
            const newArray = [ ...beforeArray, ...afterArray ];

            return {
                todoData: newArray
            }
        });
    }

    onToggleProperty = (arr, id, propName) => {
        const idx = arr.findIndex((el) => el.id === id);
        const oldItem = arr[idx];
        const newItem = { ...oldItem, [propName]: !oldItem[propName]};

        const newArr = [
            ...arr.slice(0, idx),
            newItem,
            ...arr.slice(idx + 1)
        ];

        return {
            todoData: newArr
        }
    }

    onToggleImportant = (id) => {
        this.setState(({todoData}) => this.onToggleProperty(todoData, id, 'isImportant'));
    }

    onToggleDone = (id) => {
        this.setState(({todoData}) => this.onToggleProperty(todoData, id, 'done'));
    }

    addItem = (text) => {
        this.state.todoData.push();

        const newItem = {
            name: text,
            isImportant: false,
            done: false,
            id: this.maxId++
        };

        this.setState(({todoData}) => {
            const newArr = [
                ...todoData,
                newItem
            ];

            return {
                todoData: newArr
            }
        });
    }

    render() {

        const doneCount = this.state.todoData.filter((el) => el.done).length;
        const todoCount = this.state.todoData.length - doneCount;

        return (<div className="app-todo">
            <AppHeader toDo={todoCount} done={doneCount} />
            <SearchPanel />
            <StatusFilter />
            <TodoList
                todos={this.state.todoData}
                onDeleted={this.deleteItem}
                onToggleImportant={this.onToggleImportant}
                onToggleDone={this.onToggleDone}
            />
            <AddItem onItemAdded={this.addItem} />

            <Row>
                <Col span={24}>
                    <Input
                        placeholder="Basic usage"
                        status="error"
                    />
                </Col>
            </Row>
            <Row>
                <Col span={12}>col-12</Col>
                <Col span={12}>col-12</Col>
            </Row>
            <Row>
                <Col span={8}>col-8</Col>
                <Col span={8}>col-8</Col>
                <Col span={8}>col-8</Col>
            </Row>
            <Row>
                <Col span={6}>col-6</Col>
                <Col span={6}>col-6</Col>
                <Col span={6}>col-6</Col>
                <Col span={6}>col-6</Col>
            </Row>

        </div>);
    }

}

export default App;